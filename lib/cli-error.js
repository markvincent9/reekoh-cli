'use strict'

class CliError extends Error {
  constructor (message, data = {}) {
    super(message)
    this.name = 'CliError'
    this.data = data
  }
}

module.exports = CliError
